defmodule Argumentos do
  @moduledoc """
  Documentation for `Argumentos`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Argumentos.hello()
      :world

  """
  def hello do
    :world
  end
end
