defmodule Argumentos.CLI do
  def main(args) do
    [filename] = args
    File.read!(filename)
    |> IO.puts
  end
end
