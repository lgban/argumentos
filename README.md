# Creación de una aplicación de consola (CLI - Command Line Interface)

Con esta aplicación de ejemplo, quiero mostrarles como crear una aplicación que puede ejecutarse directamente desde la línea de comando. Para esto, vamos a hacer uso de la librería de erlang llamada "escript", misma que funciona con Elixir.

## Configuración

Abra el archivo `mix.exs`, que contiene los parámetros de configuración de la aplicación. En ese archivo, agregué la línea 11, que debe verse de la siguiente manera:

```
escript: escript()
```

Esa línea hace referencia a una función (que se llama `escript/0`) misma que será tomada en cuenta por la librería `escript`. Estoy siguiendo las prácticas de otros programadores, personalmente, preferiría evitar usar el mismo nombre para evitar la confusión. La implementación de tal función se encuentra en las líneas 30-32 del mismo archivo que, por conveniencia, copié a continuación:

```
  defp escript do
    [main_module: Argumentos.CLI]
  end
```

## Implementación del CLI

Abra el archivo `lib/cli.ex`. En el encontrará un módulo llamado `Argumentos.CLI`, mismo que referimos en la sección de configuración. Encontrará que tal módulo define una función `main/1` que recibe un sólo parámetro llamado `args`, que corresponde a la lista de argumentos con el que se llamará al ejecutable de la aplicación. Esta función, simplemente regresa el punto de entrada (similar a la función `main()` en un programa en lenguaje C). A manera de ejemplo, incluí un programa que:

- Lee un archivo de texto (el nombre del archivo debe pasarse como argumento en la línea de comando)
- Imprime el contenido del archivo

Con esto, podemos generar un archivo "ejecutable". En realidad es un archivo que contiene bytecodes de la máquina virtual de Erlang, mismo que podemos ejecutar. Lo anterior semeja a generar un jar file con el bytecode de una aplicación Java.

Para general el archivo "ejecutable", use el siguiente comando:

```
mix escript.build
```

Lo anterior generará un archivo llamado `argumentos` en el folder del proyecto. Para ejecutarlo, debe usar el comando:

```
escript argumentos input.txt
```

Note que `input.txt` es el nombre de un archivo de texto que incluí en el proyecto para usarlo como ejemplo.
